var logger = loggerFactory('inline_editor')

InlineEditor = (function(){
    var Constructor = function() {
        this._editors = {};
        this._currentEditor = null;
        this.logger   = loggerFactory('InlineEditor')
    };

    Constructor.prototype.closeCurrentEditor = function(){
        if( this._currentEditor ){
            this._currentEditor.editMode.set(false);
        };
    },
    Constructor.prototype.getCurrentEditor = function(){
        return this._currentEditor;
    },

    Constructor.prototype.get = function(name){
        if(!name)
            throw new Error('Cannot get editor with name = ' + name)

        if(!this._editors[name])
            throw new Error('There is no editor with name = ' + name)

        return this._editors[name];
    }

    Constructor.prototype.add = function(name, editor){
        if(!name)
            throw new Error('Cannot add editor with name = ' + name)

        if(!editor)
            throw new Error('Cannot add editor = ' + editor)

        this.logger.log('Added editor with name = ' + name);
        this._editors[name] =  editor;
        this._currentEditor =  editor;

    };

    Constructor.prototype.remove = function(name){
        delete this._editors[name];
    }

    return {
        getInstance: function (){
            return this.instance || (this.instance = new Constructor);
        }
    }
})();

Template.InlineEditor.helpers({
    editMode:function(){
        return Template.instance().editMode.get();
    }
})

Template.InlineEditor.events({
    'click .inlineEditor_value':function(e, tmpl){
        logger.log(e, tmpl);
        InlineEditor.getInstance().closeCurrentEditor()
        tmpl.editMode.set(true);
    }
})

Template.InlineEditor.created = function(){
    this.editMode = new ReactiveVar(false);
}
Template.InlineEditor.rendered = function(){
    var self = this;
    var name = self.view.parentView.dataVar.curValue.name;

    this.autorun(function(){
        if(self.editMode.get() == true){
            try{
                InlineEditor.getInstance().add(name, self);
            }catch(e){
                logger.error(e);
            }

            var formSelector = '.inlineEditor_form';


            $("html").on('click', function (e)
            {
                logger.log(e.target)
                if (
                    $(e.target).hasClass('inlineEditor_form')  ||
                    $(e.target).parents('.inlineEditor_form').length > 0
                ){
                    // click inside inlineEditor_form
                }else{
                    $("html").off('click');
                    self.editMode.set(false);
                }

            });
        }else{
            try{
                $("html").off('click');
                InlineEditor.getInstance().remove(name);
            }catch(e){
                logger.error(e);
            }
        }
    })

}