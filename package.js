Package.describe({
  name: 'parhelium:ui-inline-editor',
  version: '0.0.1',
  summary: 'Meteor component : inline editor',
  git: '',
  documentation: 'README.md'
});

client = ['client'];
server = ['server'];
both   = ['client', 'server'];

Package.onUse(function(api) {
  api.versionsFrom('1.0.3.1');

  api.use([
      'less',
      'templating',
      'parhelium:logger',
      'reactive-var'
  ], client);

  api.addFiles( [
      'lib/inline_editor.html',
      'lib/inline_editor.js',
      'lib/inline_editor.less'
  ], client);

  api.export('InlineEditor', client)
});

